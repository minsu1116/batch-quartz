package com.lscns.schedule.application.quartz.mapper;


import com.lscns.schedule.application.model.quartz.ScheduleModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface QuartzMapper {

    public List<ScheduleModel> getList(ScheduleModel ScheduleModel) throws Exception;
}
