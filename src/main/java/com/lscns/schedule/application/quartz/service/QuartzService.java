package com.lscns.schedule.application.quartz.service;

import com.lscns.schedule.application.model.quartz.JobModel;
import com.lscns.schedule.application.model.quartz.ScheduleModel;
import com.lscns.schedule.application.quartz.job.util.JobUtil;
import com.lscns.schedule.application.quartz.mapper.QuartzMapper;
import com.lscns.schedule.application.quartz.service.interfaces.JobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class QuartzService implements JobService {

    private final Scheduler scheduler;

    public QuartzService(Scheduler scheduler) {
        this.scheduler = scheduler;
    }


    @Autowired
    private QuartzMapper quartzMapper;

    /**
     * Job 등록 및 scheduler 시작
     * @param jobModel
     * @throws Exception
     */
    @Override
    public void addJob(JobModel jobModel) throws Exception {

        // 중복 체크


        // 스케쥴 시작
        scheduler.start();

        // Job 내용 생성
        JobDetail jobDetail = JobBuilder.newJob(JobUtil.getClass(jobModel.getJobClassName()).getClass())
                                        .withIdentity(jobModel.getJobClassName(), jobModel.getJobGroupName())
                                        .withDescription(jobModel.getJobDescription())
                                        .build();


        //Cron 생성
        CronScheduleBuilder cron = CronScheduleBuilder.cronSchedule(jobModel.getCronExpression());

        // Cron으로 Trigger 생성
        CronTrigger trigger = TriggerBuilder.newTrigger()
                                            .withIdentity(jobModel.getJobClassName(), jobModel.getJobGroupName())
                                            .withSchedule(cron)
                                            .build();

        try {
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (SchedulerException e) {
            log.error("[AddJob] 생성 실패");
            throw new Exception("[AddJob] 생성 실패");
        }
    }

    /**
     * schedule 삭제
     * @param jobModel
     * @throws SchedulerException
     */
    @Override
    public void deleteJob(JobModel jobModel) throws SchedulerException {
        // trigger 중지
        scheduler.pauseTrigger(TriggerKey.triggerKey(jobModel.getJobClassName(), jobModel.getJobGroupName()));

        // 해당 Job과 관련된 모든 Trigger를 해제함
        scheduler.unscheduleJob(TriggerKey.triggerKey(jobModel.getJobClassName(), jobModel.getJobGroupName()));

        // Job을 삭제함
        scheduler.deleteJob(JobKey.jobKey(jobModel.getJobClassName(), jobModel.getJobGroupName()));
    }

    /**
     * 등록된 Job 일시 중지
     * @param jobModel
     * @throws SchedulerException
     */
    @Override
    public void pauseJob(JobModel jobModel) throws SchedulerException {
        scheduler.pauseJob(JobKey.jobKey(jobModel.getJobClassName(), jobModel.getJobGroupName()));
    }


    /**
     * 일시중지 Job 재시작
     * @param jobModel
     * @throws SchedulerException
     */
    @Override
    public void resumeJob(JobModel jobModel) throws SchedulerException {
        scheduler.resumeJob(JobKey.jobKey(jobModel.getJobClassName(), jobModel.getJobGroupName()));
    }

    /**
     * cron 변경
     * @param jobModel
     * @throws Exception
     */
    @Override
    public void cronJob(JobModel jobModel) throws Exception {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobModel.getJobClassName(), jobModel.getJobGroupName());
            
            // 변경된 시간으로 cron 생성
            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(jobModel.getCronExpression());
            
            // 등록된 cronTrigger 정보 가져오기
            CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
            
            // Trigger 재생성
            cronTrigger = cronTrigger.getTriggerBuilder()
                                     .withIdentity(triggerKey)
                                     .withSchedule(cronScheduleBuilder)
                                     .build();
            
            // 생성된 Trigger 재등록
            scheduler.rescheduleJob(triggerKey, cronTrigger);

        } catch (SchedulerException e) {
            log.error("[cronJob] 변경 실패");
            throw new Exception("[cronJob] 변경 실패");
        }
    }

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public List<ScheduleModel> getList(ScheduleModel scheduleModel) throws Exception{
        return quartzMapper.getList(scheduleModel);
    }
}
