package com.lscns.schedule.application.quartz.job.util;

import com.lscns.schedule.application.quartz.job.base.BaseJob;

public class JobUtil {

    /**
     * Job 이름으로 해당 객체 반환
     * @param classname
     * @return
     * @throws Exception
     */
    public static BaseJob getClass(String classname) throws Exception {
        Class<?> clazz = Class.forName(classname);
        return (BaseJob) clazz.newInstance();
    }
}
