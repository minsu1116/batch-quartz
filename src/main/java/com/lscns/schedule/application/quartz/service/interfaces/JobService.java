package com.lscns.schedule.application.quartz.service.interfaces;

import com.lscns.schedule.application.model.quartz.JobModel;
import com.lscns.schedule.application.model.quartz.ScheduleModel;
import org.quartz.SchedulerException;

import java.util.List;

public interface JobService {

    /**
     * Job 등록 및 시작
     * @param jobModel
     * @throws Exception
     */
    void addJob(JobModel jobModel) throws  Exception;

    /**
     * Job 삭제
     * @param jobModel
     * @throws SchedulerException
     */
    void deleteJob(JobModel jobModel) throws SchedulerException;

    /**
     * Job 중지
     * @param jobModel
     * @throws SchedulerException
     */
    void pauseJob(JobModel jobModel) throws SchedulerException;

    /**
     * Job 재시작
     * @param jobModel
     * @throws SchedulerException
     */
    void resumeJob(JobModel jobModel) throws SchedulerException;

    /**
     * cron 변경
     * @param jobModel
     * @throws Exception
     */
    void cronJob(JobModel jobModel) throws Exception;

    /**
     * Job 목록
     * @return
     */
    List<ScheduleModel> getList(ScheduleModel scheduleModel) throws Exception;
}
