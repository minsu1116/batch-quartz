package com.lscns.schedule.application.quartz.job.mail;

import com.lscns.schedule.application.csr.mail.service.MailService;
import com.lscns.schedule.application.quartz.job.base.BaseJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class MailJob implements BaseJob {

    @Autowired
    private MailService mailService;

    @Value("${spring.profiles}")
    private String profiles;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            // 운영 환경에서만 동작할 수 있도록 변경, local 개발시에는 해당 로직 주석 후 테스트
            mailService.MainProcess();
            
        } catch (Exception e) {
            log.error(e.getMessage());
        }finally {
            // 로그 insert
            try {
            }catch (Exception e) {
                log.error(e.getMessage());
            }
        }
    }
}
