package com.lscns.schedule.application.quartz.controller;

import com.lscns.schedule.application.model.quartz.JobModel;
import com.lscns.schedule.application.model.quartz.ScheduleModel;
import com.lscns.schedule.application.quartz.service.QuartzService;
import com.lscns.schedule.common.base.BaseController;
import com.lscns.schedule.common.model.ParameterWrapper;
import com.lscns.schedule.common.response.model.CommonResult;
import com.lscns.schedule.common.response.model.ListResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/quartz")
@Slf4j
public class QuartzController extends BaseController {

    @Autowired
    public QuartzService quartzService;

    /**
     * Job 등록 및 실행
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/add")
    public CommonResult addJob(HttpServletRequest request
                            , @RequestBody ParameterWrapper<JobModel> parameterWrapper ) throws Exception {

        try {
            System.out.println("/job/add : " + parameterWrapper.param.getJobGroupName());
            quartzService.addJob(parameterWrapper.param);
        } catch (Exception e) {
            return responseService.getFailResult(e.getMessage());
        }

        return responseService.getSuccessResult();
    }

    /**
     * Job 삭제
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/delete")
    public CommonResult addDelete(HttpServletRequest request
            , @RequestBody ParameterWrapper<JobModel> parameterWrapper ) throws Exception {

        JobModel jobModel = parameterWrapper.param;

        if(StringUtils.isBlank(jobModel.getJobClassName()) || StringUtils.isBlank(jobModel.getJobClassName()) ){
            return responseService.getFailResult("필수 값이 없습니다.");
        }

        quartzService.deleteJob(jobModel);

        return responseService.getSuccessResult();
    }

    /**
     * 등록된 Job 중지
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/pause")
    public CommonResult pauseJob(HttpServletRequest request
            , @RequestBody ParameterWrapper<JobModel> parameterWrapper ) throws Exception {

        JobModel jobModel = parameterWrapper.param;

        if(StringUtils.isBlank(jobModel.getJobClassName()) || StringUtils.isBlank(jobModel.getJobClassName()) ){
            return responseService.getFailResult("필수 값이 없습니다.");
        }

        quartzService.pauseJob(jobModel);

        return responseService.getSuccessResult();
    }

    /**
     * 중지된 Job 재시작
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/resume")
    public CommonResult resumeJob(HttpServletRequest request
            , @RequestBody ParameterWrapper<JobModel> parameterWrapper ) throws Exception {

        JobModel jobModel = parameterWrapper.param;

        if(StringUtils.isBlank(jobModel.getJobClassName()) || StringUtils.isBlank(jobModel.getJobClassName()) ){
            return responseService.getFailResult("필수 값이 없습니다.");
        }

        quartzService.resumeJob(jobModel);

        return responseService.getSuccessResult();
    }

    /**
     * 등록된 Job cron 변경
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/cron")
    public CommonResult cronJob(HttpServletRequest request
            , @RequestBody ParameterWrapper<JobModel> parameterWrapper ) throws Exception {

        try {
            quartzService.cronJob(parameterWrapper.param);
        } catch (Exception e) {
            return responseService.getFailResult(e.getMessage());
        }
        return responseService.getSuccessResult();
    }

    /**
     * 등록된 schedule 목록 조회
     * @param request
     * @param parameterWrapper
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/job/list")
    public ListResult<ScheduleModel> scheduleList(HttpServletRequest request
            , @RequestBody ParameterWrapper<ScheduleModel> parameterWrapper ) throws Exception {

        return responseService.getListResult(quartzService.getList(parameterWrapper.param));
    }
}
