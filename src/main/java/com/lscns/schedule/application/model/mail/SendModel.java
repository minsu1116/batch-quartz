package com.lscns.schedule.application.model.mail;

import com.lscns.schedule.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SendModel extends BaseModel {

	private int    mailId;
	private String compCd;
	private String senderEmpNo;
	private String senderMail;
	private String receiverEmpNo;
	private String receiverMail;
	private String ccMail;
	private String bccMail;
	private String mailTitle;
	private String mailMsg;
	private String mailTemplete;
	private String sendingState;
	private String sendingDt;
	private String sendingMsg;
	private String inputEmpNo;
	private String inputEmpNm;
	private String inputDt;
	private String modEmpNo;
	private String modEmpNm;
	private String modDt;
}
