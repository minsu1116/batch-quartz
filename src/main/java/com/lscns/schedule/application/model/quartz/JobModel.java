package com.lscns.schedule.application.model.quartz;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JobModel {

    /**
     * Job Class 이름
     */
    private String jobClassName;

    /**
     * Job 그룹 이름
     */
    private String jobGroupName;

    /**
     * 작업 설정 시간
     */
    private String cronExpression;

    /**
     * 작업 설명
     */
    private String jobDescription;
}
