package com.lscns.schedule.application.model.quartz;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;

@Getter
@Setter
@ToString
public class ScheduleModel {

    /**
     * Job 이름
     */
    private String jobName;

    /**
     * Job 그룹
     */
    private String jobGroup;
    /**
     * Job Class 이름 (package)
     */
    private String jobClassName;
    /**
     * 트리거 이름
     */
    private String triggerName;
    /**
     * 트리거 그룹
     */
    private String triggerGroup;
    /**
     * 반복 시간
     */
    private BigInteger repeatInterval;
    /**
     * 트리거 수
     */
    private BigInteger timesTriggered;
    /**
     * cron
     */
    private String cronExpression;
    /**
     * 시간대
     */
    private String timeZoneId;
    /**
     * 트리거 상태
     */
    private String triggerState;


    private Long  nextFireTime;
    private Long  prevFireTime;
    private Long  startTime;

    private String description;
    private String  nextFireTimeConvert;
    private String  prevFireTimeConvert;
    private String  startTimeConvert;
}
