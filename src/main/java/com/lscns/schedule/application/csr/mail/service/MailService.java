package com.lscns.schedule.application.csr.mail.service;


import com.lscns.schedule.application.model.mail.SendModel;
import com.lscns.schedule.application.csr.mail.mapper.MailMapper;
import lombok.extern.slf4j.Slf4j;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ConnectingIdType;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.misc.ImpersonatedUserId;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

@Slf4j
@Service
public class MailService {

    @Autowired
    private MailMapper mapper;

    @Value("${legacy.exchange.uri}")
    private String uri;

    @Value("${legacy.exchange.username}")
    private String username;

    @Value("${legacy.exchange.password}")
    private String password;

    @Value("${legacy.exchange.domain}")
    private String domain;

    @Value("${legacy.exchange.sender}")
    private String sender;

    /**
     * 메일 보내기 메인
     * @throws Exception
     */
    public void MainProcess() throws Exception {

        SendModel sendModel = new SendModel();

        List<SendModel> sendList = mapper.getMailSendInfo(sendModel);

        int listSize = sendList.size();


        for (int i = 0; i < listSize; i++) {
            SendModel tempModel = sendList.get(i);

            try {
                this.MailSender(tempModel);

                tempModel.setSendingState("C");
                tempModel.setSendingMsg("정상 발송");
                this.updateMailSendInfo(tempModel);

            } catch (Exception e) {
                log.error(e.getMessage());
                tempModel.setSendingState("E");
                tempModel.setSendingMsg(e.getMessage());
                this.updateMailSendInfo(tempModel);
            }
        }
    }


    /**
     * 메일 보내기 Setting
     * @param sendModel
     * @throws Exception
     */
    public void MailSender(SendModel sendModel) throws Exception {

        JSONArray receiverArray = new JSONArray(sendModel.getReceiverMail());
        JSONArray ccArray = new JSONArray();

        if(sendModel.getCcMail() != null && !sendModel.getCcMail().equals("")){
            ccArray = new JSONArray(sendModel.getCcMail());
        }

        ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        ExchangeCredentials credentials = new WebCredentials(sender, password);
        service.setCredentials(credentials);
        service.setUrl(new URI(uri));


        EmailMessage msg = new EmailMessage(service);

        msg.setSubject(String.valueOf(sendModel.getMailTitle()));
        msg.setBody(MessageBody.getMessageBodyFromText(this.getMakedBody(sendModel)));

        InputStreamReader inputStreamReader = null;
        FileInputStream fileInputStream = null;
        ClassPathResource resource = null;

        switch (sendModel.getMailTemplete()) {
            case "service_req_review.html" :
            case "service_req_accept.html":
            case "service_req_proc.html":
            case "service_req_re_proc.html":
            case "service_req_complete.html":
                break;
        }

        for(int j = 0; j < receiverArray.length(); j++) {

            JSONObject receiver = receiverArray.getJSONObject(j);
            msg.getToRecipients().add(receiver.get("email").toString());
        }

        for(int j = 0; j < ccArray.length(); j++) {
            JSONObject cc = ccArray.getJSONObject(j);
            msg.getCcRecipients().add(cc.get("email").toString());
        }

        msg.send();
    }

    /**
     * EMAIL 내용
     * @return
     */
    public String getMakedBody(SendModel sendModel) throws Exception {
        String content = null;
        BufferedReader br = null;

        if (!sendModel.getMailTemplete().equals("")) {

            InputStreamReader inputStreamReader = null;
            FileInputStream fileInputStream = null;
            ClassPathResource resource = null;

            try {

                String resourcePath = "templates/" + sendModel.getMailTemplete();
                resource = new ClassPathResource(resourcePath);
                byte[] bdata = FileCopyUtils.copyToByteArray(resource.getInputStream());

                JSONArray contentsArray = new JSONArray(sendModel.getMailMsg());

                String reqDtm = "", reqEmpNm = "", reqTitle = "", dtlInfo = "", categoryAll="", endDemandDt = "", shortcuts = "", procDtm = "", procGubnNm = "", procEmpNm = "", procTelNo = "", returnDtm = "", returnContents = "",
                       rejectDtm = "", rejectEmpNm = "", rejectTelNo = "", rejectContents = "";
                switch (sendModel.getMailTemplete()) {
                    case "service_req_review.html":
                    case "service_req_accept.html":
                    case "service_req_proc.html":
                        for (int i = 0; i < contentsArray.length(); i++) {
                            reqDtm = contentsArray.getJSONObject(i).get("reqDtm").toString();
                            reqEmpNm = contentsArray.getJSONObject(i).get("reqEmpNm").toString();
                            reqTitle = contentsArray.getJSONObject(i).get("reqTitle").toString();
                            categoryAll = contentsArray.getJSONObject(i).get("categoryAll").toString();
                            endDemandDt = contentsArray.getJSONObject(i).get("endDemandDt").toString();
                            shortcuts = contentsArray.getJSONObject(i).get("shortcuts").toString();
                        }

                        content = new String(bdata, "utf-8");
                        content = content.replace("{reqDtm}", String.valueOf(reqDtm).replace("null", ""));
                        content = content.replace("{reqEmpNm}", String.valueOf(reqEmpNm).replace("null", ""));
                        content = content.replace("{reqTitle}", String.valueOf(reqTitle).replace("null", ""));
                        content = content.replace("{categoryAll}", String.valueOf(categoryAll).replace("null", ""));
                        content = content.replace("{endDemandDt}", String.valueOf(endDemandDt).replace("null", ""));
                        content = content.replace("{shortcuts}", String.valueOf(shortcuts).replace("null", ""));

                        break;
                    case "service_req_re_proc.html":
                        for (int i = 0; i < contentsArray.length(); i++) {
                            reqDtm = contentsArray.getJSONObject(i).get("reqDtm").toString();
                            reqEmpNm = contentsArray.getJSONObject(i).get("reqEmpNm").toString();
                            reqTitle = contentsArray.getJSONObject(i).get("reqTitle").toString();
                            categoryAll = contentsArray.getJSONObject(i).get("categoryAll").toString();
                            endDemandDt = contentsArray.getJSONObject(i).get("endDemandDt").toString();
                            shortcuts = contentsArray.getJSONObject(i).get("shortcuts").toString();
                            returnDtm = contentsArray.getJSONObject(i).get("returnDtm").toString();
                            returnContents = contentsArray.getJSONObject(i).get("returnContents").toString();
                        }

                        content = new String(bdata, "utf-8");
                        content = content.replace("{reqDtm}", String.valueOf(reqDtm).replace("null", ""));
                        content = content.replace("{reqEmpNm}", String.valueOf(reqEmpNm).replace("null", ""));
                        content = content.replace("{reqTitle}", String.valueOf(reqTitle).replace("null", ""));
                        content = content.replace("{categoryAll}", String.valueOf(categoryAll).replace("null", ""));
                        content = content.replace("{endDemandDt}", String.valueOf(endDemandDt).replace("null", ""));
                        content = content.replace("{shortcuts}", String.valueOf(shortcuts).replace("null", ""));
                        content = content.replace("{returnDtm}", String.valueOf(returnDtm).replace("null", ""));
                        content = content.replace("{returnContents}", String.valueOf(returnContents).replace("null", ""));

                        break;
                    case "service_req_complete.html":
                        for (int i = 0; i < contentsArray.length(); i++) {
                            reqDtm = contentsArray.getJSONObject(i).get("reqDtm").toString();
                            reqEmpNm = contentsArray.getJSONObject(i).get("reqEmpNm").toString();
                            reqTitle = contentsArray.getJSONObject(i).get("reqTitle").toString();
                            categoryAll = contentsArray.getJSONObject(i).get("categoryAll").toString();
                            endDemandDt = contentsArray.getJSONObject(i).get("endDemandDt").toString();
                            shortcuts = contentsArray.getJSONObject(i).get("shortcuts").toString();
                            procDtm = contentsArray.getJSONObject(i).get("procDtm").toString();
                            procGubnNm = contentsArray.getJSONObject(i).get("procGubnNm").toString();
                            procEmpNm = contentsArray.getJSONObject(i).get("procEmpNm").toString();
                            procTelNo = contentsArray.getJSONObject(i).get("procTelNo").toString();
                        }

                        content = new String(bdata, "utf-8");
                        content = content.replace("{reqDtm}", String.valueOf(reqDtm).replace("null", ""));
                        content = content.replace("{reqEmpNm}", String.valueOf(reqEmpNm).replace("null", ""));
                        content = content.replace("{reqTitle}", String.valueOf(reqTitle).replace("null", ""));
                        content = content.replace("{categoryAll}", String.valueOf(categoryAll).replace("null", ""));
                        content = content.replace("{endDemandDt}", String.valueOf(endDemandDt).replace("null", ""));
                        content = content.replace("{shortcuts}", String.valueOf(shortcuts).replace("null", ""));
                        content = content.replace("{procDtm}", String.valueOf(procDtm).replace("null", ""));
                        content = content.replace("{procGubnNm}", String.valueOf(procGubnNm).replace("null", ""));
                        content = content.replace("{procEmpNm}", String.valueOf(procEmpNm).replace("null", ""));
                        content = content.replace("{procTelNo}", String.valueOf(procTelNo).replace("null", ""));


                        break;
                    case "service_req_reject.html":
                        for (int i = 0; i < contentsArray.length(); i++) {
                            reqDtm = contentsArray.getJSONObject(i).get("reqDtm").toString();
                            reqEmpNm = contentsArray.getJSONObject(i).get("reqEmpNm").toString();
                            reqTitle = contentsArray.getJSONObject(i).get("reqTitle").toString();
                            categoryAll = contentsArray.getJSONObject(i).get("categoryAll").toString();
                            endDemandDt = contentsArray.getJSONObject(i).get("endDemandDt").toString();
                            shortcuts = contentsArray.getJSONObject(i).get("shortcuts").toString();
                            rejectDtm = contentsArray.getJSONObject(i).get("rejectDtm").toString();
                            rejectContents = contentsArray.getJSONObject(i).get("rejectContents").toString();
                            rejectTelNo = contentsArray.getJSONObject(i).get("rejectTelNo").toString();
                            rejectEmpNm = contentsArray.getJSONObject(i).get("rejectEmpNm").toString();
                        }

                        content = new String(bdata, "utf-8");
                        content = content.replace("{reqDtm}", String.valueOf(reqDtm).replace("null", ""));
                        content = content.replace("{reqEmpNm}", String.valueOf(reqEmpNm).replace("null", ""));
                        content = content.replace("{reqTitle}", String.valueOf(reqTitle).replace("null", ""));
                        content = content.replace("{categoryAll}", String.valueOf(categoryAll).replace("null", ""));
                        content = content.replace("{endDemandDt}", String.valueOf(endDemandDt).replace("null", ""));
                        content = content.replace("{shortcuts}", String.valueOf(shortcuts).replace("null", ""));
                        content = content.replace("{rejectDtm}", String.valueOf(rejectDtm).replace("null", ""));
                        content = content.replace("{rejectContents}", String.valueOf(rejectContents).replace("null", ""));
                        content = content.replace("{rejectEmpNm}", String.valueOf(rejectEmpNm).replace("null", ""));
                        content = content.replace("{rejectTelNo}", String.valueOf(rejectTelNo).replace("null", ""));


                        break;
                }
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            } finally {
                if (fileInputStream != null)
                    try {
                        fileInputStream.close();
                    } catch (Exception e2) {
                    }
                if (inputStreamReader != null)
                    try {
                        inputStreamReader.close();
                    } catch (Exception e2) {
                    }
                if (br != null)
                    try {
                        br.close();
                    } catch (Exception e2) {
                    }
            }

        } else {
            content = sendModel.getMailMsg();
        }
        return content;

    }

    /**
     * 메일 전송 후 상태값 업데이트
     * @param model
     * @throws Exception
     */
    public void updateMailSendInfo(SendModel model) throws Exception{
        mapper.updateMailSendInfo(model);
    }

}
