package com.lscns.schedule.application.csr.mail.mapper;


import com.lscns.schedule.application.model.mail.SendModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MailMapper {
    public List<SendModel> getMailSendInfo(SendModel model) throws Exception;
    public void updateMailSendInfo(SendModel model) throws Exception;
}
