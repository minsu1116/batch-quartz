package com.lscns.schedule.application.csr.hr.service;


import com.lscns.schedule.application.csr.hr.mapper.HRMapper;
import com.lscns.schedule.application.model.mail.SendModel;
import lombok.extern.slf4j.Slf4j;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

@Slf4j
@Service
public class HRService {

    @Autowired
    private HRMapper hrMapper;



    /**
     * HR 동기화 메인
     * @throws Exception
     */
    public void MainProcess() throws Exception {
        hrMapper.saveHRUsers();
        hrMapper.saveHRDepts();
    }

}
