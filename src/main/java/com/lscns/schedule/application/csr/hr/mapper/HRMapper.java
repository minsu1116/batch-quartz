package com.lscns.schedule.application.csr.hr.mapper;


import com.lscns.schedule.application.model.mail.SendModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface HRMapper {

    public void saveHRUsers() throws Exception;
    public void saveHRDepts() throws Exception;

}
