package com.lscns.schedule.config.db.csr;

import com.lscns.schedule.config.db.HikariMainProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.csr.datasource")
public class Hikari2Properties extends HikariMainProperties {
}
