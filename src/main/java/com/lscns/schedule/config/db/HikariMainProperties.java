package com.lscns.schedule.config.db;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HikariMainProperties extends DataSourceProperties{
	private int maximumPoolSize;
	private int connectionTimeOut;
}

