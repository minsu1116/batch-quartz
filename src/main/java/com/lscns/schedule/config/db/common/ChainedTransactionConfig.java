package com.lscns.schedule.config.db.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class ChainedTransactionConfig {

	@Primary
	@Bean
	public PlatformTransactionManager transactionManager(
			PlatformTransactionManager db1TxManager
	){
		return new ChainedTransactionManager(db1TxManager);
	}


}
