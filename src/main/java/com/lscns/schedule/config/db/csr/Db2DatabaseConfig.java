package com.lscns.schedule.config.db.csr;

import com.lscns.schedule.config.db.DataSourceCreator;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties(Hikari2Properties.class)
@MapperScan(value="com.lscns.schedule.application.csr", sqlSessionFactoryRef="db2SqlSessionFactory")
public class Db2DatabaseConfig {

    @Bean(name = "db2DataSource")
    public DataSource db2DataSource(Hikari2Properties properties) {
        return DataSourceCreator.createHikariDataSource(properties);
    }


    @Bean
    public PlatformTransactionManager db2TxManager(DataSource db2DataSource) {
        return new DataSourceTransactionManager(db2DataSource);
    }

    @Bean(name = "db2SqlSessionFactory")
    public SqlSessionFactory db2SqlSessionFactory(DataSource db2DataSource) throws Exception {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setVfs(SpringBootVFS.class);
        sqlSessionFactoryBean.setDataSource(db2DataSource);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setTypeAliasesPackage("com.lscns.schedule.application.model");
        sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:com/lscns/schedule/application/csr/**/*-Sql.xml"));

        org.apache.ibatis.session.Configuration mybatisconfig = new org.apache.ibatis.session.Configuration();
        mybatisconfig.setMapUnderscoreToCamelCase(true);
        sqlSessionFactoryBean.setConfiguration(mybatisconfig);

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "db2SqlSessionTemplate")
    public SqlSessionTemplate db2SqlSessionTemplate(SqlSessionFactory db2SqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(db2SqlSessionFactory);
    }

    @Bean(destroyMethod = "clearCache")
    public SqlSession db2SqlSession(SqlSessionFactory db2SqlSessionFactory) {
        return new SqlSessionTemplate(db2SqlSessionFactory);
    }


}
