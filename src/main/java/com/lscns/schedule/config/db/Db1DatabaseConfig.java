package com.lscns.schedule.config.db;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.boot.autoconfigure.SpringBootVFS;
import org.springframework.boot.autoconfigure.quartz.SchedulerFactoryBeanCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableConfigurationProperties(Hikari1Properties.class)
@MapperScan(value="com.lscns.schedule.application.quartz", sqlSessionFactoryRef="db1SqlSessionFactory")
public class Db1DatabaseConfig {

	@Bean(name = "db1DataSource")
    public DataSource db1DataSource(Hikari1Properties properties) {
        return DataSourceCreator.createHikariDataSource(properties);
    }


    @Bean
    public PlatformTransactionManager db1TxManager(DataSource db1DataSource) {
        return new DataSourceTransactionManager(db1DataSource);
    }

    @Bean(name = "db1SqlSessionFactory")
    public SqlSessionFactory db1SqlSessionFactory(DataSource db1DataSource) throws Exception {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setVfs(SpringBootVFS.class);
        sqlSessionFactoryBean.setDataSource(db1DataSource);

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sqlSessionFactoryBean.setTypeAliasesPackage("com.lscns.schedule.application.model");
        sqlSessionFactoryBean.setMapperLocations(resolver.getResources("classpath*:com/lscns/schedule/application/quartz/**/*-Sql.xml"));

        org.apache.ibatis.session.Configuration mybatisconfig = new org.apache.ibatis.session.Configuration();
        mybatisconfig.setMapUnderscoreToCamelCase(true);
        sqlSessionFactoryBean.setConfiguration(mybatisconfig);

        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "db1SqlSessionTemplate")
    public SqlSessionTemplate db1SqlSessionTemplate(SqlSessionFactory db1SqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(db1SqlSessionFactory);
    }

    @Bean(destroyMethod = "clearCache")
    public SqlSession db1SqlSession(SqlSessionFactory db1SqlSessionFactory) {
        return new SqlSessionTemplate(db1SqlSessionFactory);
    }

    @Bean
    public SchedulerFactoryBeanCustomizer schedulerFactoryBeanCustomizer(DataSource db1DataSource,PlatformTransactionManager db1TxManager)
    {
        return bean ->
        {
            bean.setDataSource(db1DataSource);
            bean.setTransactionManager(db1TxManager);
        };
    }


}
