package com.lscns.schedule.config.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.RuleBasedTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionInterceptor;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Configuration
@Slf4j
public class Db1TransactionConfig {

    @Autowired
    private PlatformTransactionManager platformTransactionManager;

    /**
     * setTimeout : 60
     * get :
     * insert, save, update, delete
     * db1DataSourceTransactionManager
     * @return
     */
    @Bean
    public TransactionInterceptor txAdvice1() {
    	TransactionInterceptor txAdvice = new TransactionInterceptor();
       	Properties txAttributes = new Properties();

       	// RollBack Rules
    	List<RollbackRuleAttribute> rollbackRules = new ArrayList<RollbackRuleAttribute>();
    	rollbackRules.add(new RollbackRuleAttribute(Exception.class));

    	// 읽기 전용
    	DefaultTransactionAttribute readOnlyAttribute = new DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRED);
    	readOnlyAttribute.setReadOnly(true);
    	readOnlyAttribute.setTimeout(60);
    	readOnlyAttribute.setIsolationLevel(TransactionDefinition.ISOLATION_READ_UNCOMMITTED);

    	// 나머지
        RuleBasedTransactionAttribute writeAttribute = new RuleBasedTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRED, rollbackRules);
        writeAttribute.setTimeout(60);
        writeAttribute.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);

		String readOnlyTransactionAttributesDefinition = readOnlyAttribute.toString();
		String writeTransactionAttributesDefinition = writeAttribute.toString();

		log.info("Read Only Attributes :: {}", readOnlyTransactionAttributesDefinition);
		// Read Only Attributes :: PROPAGATION_REQUIRED,ISOLATION_DEFAULT,timeout_60,readOnly
		log.info("Write Attributes :: {}", writeTransactionAttributesDefinition);
		// Write Attributes :: PROPAGATION_REQUIRED,ISOLATION_DEFAULT,timeout_60,-java.lang.Exception

		txAttributes.setProperty("get*", readOnlyTransactionAttributesDefinition);
		txAttributes.setProperty("insert*", writeTransactionAttributesDefinition);
		txAttributes.setProperty("save*", writeTransactionAttributesDefinition);
		txAttributes.setProperty("update*", writeTransactionAttributesDefinition);
		txAttributes.setProperty("delete*", writeTransactionAttributesDefinition);

		txAdvice.setTransactionAttributes(txAttributes);
		txAdvice.setTransactionManager(platformTransactionManager);
		txAdvice.setTransactionManagerBeanName("db1TxManager");
        return txAdvice;
    }

    @Bean(name = "txAdvice_db1")
    public Advisor txAdviceAdvisor() {

        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("execution(* com.lscns.schedule.application.quartz..*.service.*Service.*(..))");
        return new DefaultPointcutAdvisor(pointcut, txAdvice1());
    }

}
