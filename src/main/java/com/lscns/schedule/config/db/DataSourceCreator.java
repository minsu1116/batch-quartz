package com.lscns.schedule.config.db;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;

import com.zaxxer.hikari.HikariDataSource;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class DataSourceCreator {

    public DataSource createHikariDataSource(HikariMainProperties properties) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(properties.getUrl());
        dataSource.setUsername(properties.getUsername());
        dataSource.setPassword(properties.getPassword());
        dataSource.setDriverClassName(properties.getDriverClassName());

        dataSource.setMaximumPoolSize(properties.getMaximumPoolSize());
        dataSource.setConnectionTimeout(properties.getConnectionTimeOut());

        return new LazyConnectionDataSourceProxy(dataSource);
    }


    /**
     *HikariCP의 property 설정은 다음과 같습니다.
     *
	     1. autoCommit (default : true)
	     : connection이 종료되거나 pol에 반환될 때, connection에 속해있는 transaction을 commit 할지를 결정합니다.

	     2. readOnly (default : false)
	     : database connection을 readOnly mode로 open합니다.
	               이 설정은 database에서 지원하지 않는다면 readOnly가 아닌 상태로 open되기 때문에, 지원되는 database 목록을 확인해보고 사용해야지 됩니다.

	     3. transactionIsolation (default : none)
	     : java.sql.Connection 에 지정된 Transaction Isolation을 지정합니다. 지정된 Transaction Isoluation은 다음과 같습니다.
	       Connection.TRANSACTION_NONE : transaction을 지원하지 않습니다.
	       Connection.TRANSACTION_READ_UNCOMMITTED : transaction이 끝나지 않았을 때, 다른 transaction에서 값을 읽는 경우 commit되지 않은 값(dirty value)를 읽습니다.
	       Connection.TRANSACTION_READ_COMMITTED : transaction이 끝나지 않았을 때, 다른 transaction에서 값을 읽는 경우 변경되지 않은 값을 읽습니다.
	       Connection.TRANSACTION_REPEATABLE_READ : 같은 transaction내에서 값을 또다시 읽을 때, 변경되기 전의 값을 읽습니다. TRANSACTION_READ_UNCOMMITTED 와 같이 사용될 수 없습니다.
	       Connection.TRANSACTION_SERIALIZABLE : dirty read를 지원하고, non-repeatable read를 지원합니다.
	               기본값을 각 Driver vendor의 JDBCDriver에서 지원하는 Transaction Isoluation을 따라갑니다. (none으로 설정시.)

	     4. category (default : none)
	     : connection에서 연결할 category를 결정합니다. 값이 설정되지 않는 경우, JDBC Driver에서 설정된 기본 category를 지정하게 됩니다.

	     5. connectionTimeout(default: 30000 - 30 seconds)
	     : connection 연결시도시 timeout out값을 설정합니다. 이 시간내로 connection을 연결하는데 실패하면 SQLException을 발생합니다.

	     6. idleTimeout(default : 600000 - 10 minutes)
	     : connection Pool에 의하여 확보된 connection의 maximum idle time을 결정합니다.
	       connection Pool에 의하여 확보된 connection이 사용되지 않고, Pool에 의해서만 이 시간동안 관리된 경우, connection을 DB에 반환하게 됩니다.
	                값을 0으로 설정하는 경우, 확보된 connection을 절대 반환하지 않습니다.

	     7. maxLifetime(default : 1800000 - 30 minutes)
	     : connection Pool에 의하여 확보된 connection의 최대 생명주기를 지정합니다.
	       connection을 얻어낸지, 이 시간이상되면 최근에 사용하고 있던 connection일지라도, connection을 close시킵니다.
	               사용중에 있던 connection은 close 시키지 않습니다. (사용이 마쳐지면 바로 close 됩니다.)
	       HikariCP에서는 이 값을 30~60 minutes 사이의 값을 설정하라고 강력권고합니다. 값을 0로 설정하는 경우 lifetime은 무제한이 됩니다.

	     8. leakDetectionThreshold (default : 0)
	     : connectionPool에서 반환된 connection의 올바른 반환이 이루어졌는지를 확인하는 thread의 갯수를 지정합니다.
	               이 값을 0로 지정하는 경우, leak detection을 disable 시키게 됩니다.
	               만약에 또다른 connection pool을 사용하고 있다면, 다른 connection pool에서 만들어진 connection을 leak으로 판단하고 connection을 닫아버릴 수 있습니다.

		 9. jdbc4ConnectionTest (default : true) -- 최신버젼에서 없어졌습니다.
		 : connection을 맺은다음, Connection.isValid() method를 호출해서 connection이 정상적인지를 확인합니다.
		      이 property는 다음에 나올 connectionTestQuery에 매우 밀접한 영향을 받습니다.

		 10. connectionTestQuery (default : none)
		 : Connection.isValid() method를 지원하지 않는 ‘legacy’ database를 위한 빠른 query를 지정합니다.
		   (ex: VALUES 1) jdbc4ConnectionTest가 더 유용하기 때문에 사용하지 않는 것이 좋습니다.

		 11. connectionInitSql (default : none)
		 : 새로운 connection이 생성되고, Pool에 추가되기 전에 실행될 SQL query를 지정합니다.

		 12. dataSourceClassName (default : none)
		 : JDBC driver에서 지원되는 dataSourceClassName을 지정합니다. 이 값은 driverClassName이 지정된 경우, 지정할 필요가 없습니다.

		 13. dataSource (default : none)
		 : 사용자가 만든 dataSource를 Pool에 의하여 wrapped하는 것을 원하는 경우, 이 값을 지정하여 사용합니다.
		   HikariCP는 이 문자열을 이용해서 reflection을 통해 dataSource를 생성합니다. 이 값이 설정되는 경우, dataSourceClassName, driverClassName 과 같은 값들은 모두 무시 됩니다.

		 14. driverClassName
		 : HikariCP에서 사용할 DriverClass를 지정합니다. 이 값이 지정되는 경우, jdbcUrl이 반드시 설정되어야지 됩니다.

		 15. jdbcUrl
		 : jdbcUrl을 지정합니다. driverClassName이 지정된 경우, jdbcUrl을 반드시 지정해줘야지 됩니다.

		 16. minimumIdle (default : maximumPoolSize)
		 : connection Pool에서 유지할 최소한의 connection 갯수를 지정합니다. HikariCP에서는 최고의 performance를 위해
		   maximumPoolSize와 minimumIdle값을 같은 값으로 지정해서 connection Pool의 크기를 fix하는 것을 강력하게 권장합니다.

		 17. maximumPoolSize
		 : connection Pool에서 사용할 최대 connection 갯수를 지정합니다.
		       이 부분은 운영환경과 개발환경에 매우 밀접하게 연결되는 부분으로, 많은 테스트 및 운영이 필요합니다.

		 18. username
		 : Connection을 얻어내기 위해서 사용되는 인증 이름을 넣습니다.

		 19. password
		 : username과 쌍이 되는 비밀번호를 지정합니다.

		 20. poolName (default : auto-generated)
		 : logging과 JMX management에서 지정할 pool의 이름을 지정합니다.

		 21. registerMbeans (default : false)
		 : JMX management Beans에 등록되는 될지 여부를 지정합니다.
     */

}
