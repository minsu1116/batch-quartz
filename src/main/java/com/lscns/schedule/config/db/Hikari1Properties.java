package com.lscns.schedule.config.db;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.db1.datasource")
public class Hikari1Properties extends HikariMainProperties {
}
