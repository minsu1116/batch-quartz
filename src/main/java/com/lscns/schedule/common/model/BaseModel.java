package com.lscns.schedule.common.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import org.springframework.util.StringUtils;

import java.util.List;


@Getter
@Setter
public class BaseModel {

    private String bPrgFn;

    private String bSqlMessage;

    // 사용자 아이디
    private String bUserId;

    @Id
    // 사번 번호
    private String bEmpNo;

    // 사용자 이름
    private String bUserNm;

    // 패스워드
    private String bPassword;

    // 패스워드 수정 일시
    private String bPasswordModDtm;

    // 회사 코드
    private String bCompCd;

    // 회사 이름
    private String bCompNm;

    // 부서 코드
    private String bDeptCd;

    // 부서 이름
    private String bDeptNm;

    // 핸드폰 번호
    private String bMobileNo;

    // 사무실 번호
    private String bOfficeNo;

    // 이메일
    private String bEmail;

    // 직책 이름
    private String bJobNm;

    // 직책 코드
    private String bJobDuty;

    // 직급 이름
    private String bRankNm;

    // 재직 코드
    private String bWstatCd;

    // 재직 이름
    private String bWstatNm;

    // 사용 여부
    private String bUseYn;

    // HR 여부
    private String bHrYn;

    // Spring Security에서 사용하는 권한 그룹
    private List<String> bRoles;

    // Client에서 접속 시 사용하는 토큰
    private String accessToken;

    // Client에서 요청하는 디바이스 종류
    private String bDeviceType;

    // 언어 종류
    private String bLocale;

    // 패스워드 key
    private String bSalt;

    // PC 관리자 권한
    private String bPcAdminAuth;

    // SYSTEM 관리자 여부
    private Boolean bIsSystem;

   public void setBaseModel(BaseModel param)
    {
        this.bUserId = param.getBUserId();
        this.bEmpNo = param.getBEmpNo();
        this.bUserNm = param.getBUserNm();
        this.bPasswordModDtm = param.getBPasswordModDtm();
        this.bCompCd = param.getBCompCd();
        this.bCompNm = param.getBCompNm();
        this.bDeptCd = param.getBDeptCd();
        this.bDeptNm = param.getBDeptNm();
        this.bMobileNo = param.getBMobileNo();
        this.bOfficeNo = param.getBOfficeNo();
        this.bEmail = param.getBEmail();
        this.bJobNm = param.getBJobNm();
        this.bJobDuty = param.getBJobDuty();
        this.bRankNm = param.getBRankNm();
        this.bWstatCd = param.getBWstatCd();
        this.bWstatNm = param.getBWstatNm();
        this.bUseYn = param.getBUseYn();
        this.bHrYn = param.getBHrYn();
        this.bDeviceType = param.getBDeviceType();
        this.bPcAdminAuth = param.getBPcAdminAuth();
    }

    public void setbSqlMessage(String bSqlMessage) throws Exception {
        if(!StringUtils.isEmpty(bSqlMessage)) {
            throw new Exception(bSqlMessage, new Throwable(bSqlMessage));
        }else {
            this.bSqlMessage = bSqlMessage;
        }
    }

}

