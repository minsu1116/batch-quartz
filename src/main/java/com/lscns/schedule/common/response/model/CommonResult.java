package com.lscns.schedule.common.response.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {

    private boolean success; // true/false
    private int code;        // "응답 코드 번호 : > 0 정상, < 0 비정상"
    private String msg;      // "응답 메시지"
    private String token;
}
