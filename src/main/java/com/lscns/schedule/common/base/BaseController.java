package com.lscns.schedule.common.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lscns.schedule.common.response.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class BaseController {

    @Autowired
    public ResponseService responseService;

    public Map resultToMap(String result) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(result, Map.class);
    }

    public List<Map> resultToListMap(String result) throws  Exception{
        ObjectMapper mapper = new ObjectMapper();
        return Arrays.asList(mapper.readValue(result,Map[].class));
    }
}
